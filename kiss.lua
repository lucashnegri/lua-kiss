local header = [[
void luakiss_fft(int nfft, int is_inverse, void* in, void* out);
]]

local ffi = require('ffi')
ffi.cdef(header)
return ffi.load('luakiss')
