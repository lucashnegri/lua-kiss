#include "kiss_fft.c"

// needed for the FFI
void luakiss_fft(int nfft, int is_inverse, kiss_fft_cpx* in, kiss_fft_cpx* out){
    kiss_fft_cfg cfg = kiss_fft_alloc(nfft, is_inverse, 0, 0);
    kiss_fft(cfg, in, out);
    free(cfg);
}
