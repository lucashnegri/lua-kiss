luakiss
=======

Thin wrapper for KissFFT, allowing one to use LuaJIT FFI to use KissFFT inside
Lua, as KissFFT is not distributed as a library. It alsos uses double instead
of float to match Lua number type.

All the work is performed by the great (and simple!), KissFFT [1].

Usage
-----

    kiss = require('kiss')
    ffi  = require('ffi')

    in  = ffi.new('complex[4]', {0,1,2,3})
    out = ffi.new('complex[4]')
    kiss.luakiss_fft(in, out, false) -- true for inverse

License
-------

Same license as KissFFT (Revised BSD License).

[1]: http://kissfft.sourceforge.net
